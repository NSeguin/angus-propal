# Angus-propal

Rédaction de la proposition de l'équipe Inria **Angus**, qui inclut à part moi

- Daniele Di Pietro,
- Matthieu Hillairet,
- Hélène Mathis,
- François Vilar.

Il n'y a qu'un seul document à fournir, qui sera Angus_propal.pdf.
