# Retour Hélène

Section 2.1 : en toute fin de section sont mentionnés les partenaires industriels.
Je mettrais juste une phrase pour préciser que les chercheurs d’EDF et du CEA (en tout cas les applications qu’ils regardent) sont des interlocuteurs privilégiés des membres de l’équipe.
Peut-être que c’est une mauvaise idée, surtout du point de vue politique…

Session 2.2 : le texte me convient. Après la phrase "This has allowed to obtain original descriptions of the evolution of the topology of bubbles », j’ajouterais une référence de l’équipe de Samuel (par ex. la thèse de Cordesse ou Di Battista).

Section 2.3.2 :  dans la partie Compressible Multiphase Flows, on peut ajouter à la liste les travaux  de Pshkov et Romenski qui développent une stratégie hamiltonienne/équations d’Euler-Lagrange, y compris du point de vue numérique avec Dumbser.

Section 3 : j’aime bien. Ca ne me semble pas être de la science fiction.

Program : dans le medium term program, j’ajouterai comme objectif la construction de lois d’état physiquement réalistes et préservant les contraintes de convexité par interpolation à la O. Les ou par des approches numériques de géométrie de contact (il faut citer alors la thèse de L. Benayoun, Méthodes géométriques pour l'étude des systèmes thermodynamiques et la génération d'équations d’état, 1999, Grenoble IGNP).

Section 4 :

- personnellement, je n’ai pas besoin d’un recrutement IR pour le développement. Je n’en fais pas suffisamment pour que ça ait du sens.

Section 5 :

- Academic collaborations
	- Berenice Grec, Fred Herau : we address the derivation of a two-phase flow model from a kinetic formulation (une demande ANR est en cours, Fred. Charles en est la PI et Matthieu en fait partie).
	- Le projet France 2030 : In November, Hélène Mathis was awarded the Défi Mathématiques France 2030 prize during the Assises des Mathématiques. This prize, amounting to 25 K euros and a thesis grant, is associated with the project for the development of physically relevant models and robust numerical methods for the simulation of two-phase flows occurring in nuclear safety accidents. The project includes members of the team (namely M. Hillairet, N. Seguin, F. Vilar) and 4 external collaborators, including 3 industrial collaborators from EDF and CEA..
                